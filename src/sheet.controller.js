angular
    .module('aetherworlds.sheet')
    .controller('sheetController', ['$scope', 'io', function ($scope, io) {
        Object.assign($scope, require('./data.json'));

        $scope.sheet = require('./default.character.json');
        $scope.aux = {
            hitpointsBase: function () { return $scope.sheet.stats.muscle + $scope.sheet.stats.nimble + 10; }
        };

        $scope.addMove = function (name, move) {
            if (name && move) {
                $scope.sheet.moves.push({
                    name: name,
                    move: move
                });
            }
        };

        $scope.removeMove = function (index) {
            $scope.sheet.moves.splice(index, 1);
        };

        $scope.save = function () {
            io.save($scope.sheet);
        };

        $scope.load = function () {
            io.load(function (sheet) {
                $scope.$apply(function () {
                    console.log('Loaded sheet', sheet);
                    $scope.sheet = sheet;
                });
            });
        };

        $scope.print = function () {
            window.print();
        };

        window.explain = function () { console.log($scope.sheet); };
    }]);
