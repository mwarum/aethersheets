const tmpl = require('./partials/move.html');

angular
    .module('aetherworlds.sheet')
    .directive('aeMove', function () {
        return {
            restrict: 'E',
            scope: {
                aeName: '<',
                aeMove: '<'
            },
            template: tmpl
        };
    });
