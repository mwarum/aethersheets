angular
    .module('aetherworlds.sheet')
    .directive('aeVersion', ['version', function (version) {
        return {
            restrict: 'E',
            template: version
        }
    }]);
