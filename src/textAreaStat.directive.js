const tmpl = require('./partials/textAreaStat.html');

angular
    .module('aetherworlds.sheet')
    .directive('aeTextAreaStat', function () {
        return {
            restrict: 'E',
            scope: {
                name: '@',
                value: '='
            },
            template: tmpl
        };
    });
