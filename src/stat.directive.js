const tmpl = require('./partials/stat.html');

angular
    .module('aetherworlds.sheet')
    .directive('aeStat', function () {
        return {
            restrict: 'E',
            scope: {
                name: '@',
                value: '=',
                min: '@',
                max: '@'
            },
            template: tmpl
        };
    });
