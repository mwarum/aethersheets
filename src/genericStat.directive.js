const tmpl = require('./partials/genericStat.html');

angular
    .module('aetherworlds.sheet')
    .directive('aeGenericStat', function () {
        return {
            restrict: 'E',
            scope: {
                name: '@',
                value: '=',
            },
            template: tmpl,
            transclude: true
        };
    });
