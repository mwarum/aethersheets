angular
    .module('aetherworlds.sheet')
    .factory('io', function () {
        return {
            save: function (sheet) {
                var payload = JSON.stringify(angular.copy(sheet));
                var element = document.createElement('a');
                var filename = (sheet.name || 'character') + '-sheet_' + Date.now() + '.json';
                element.setAttribute('href', 'data:application/json;charset=utf-8,' + encodeURIComponent(payload));
                element.setAttribute('download', filename);

                element.style.display = 'none';
                document.body.appendChild(element);

                element.click();

                document.body.removeChild(element);
            },

            load: function (callback) {
                var input = document.createElement('input');
                input.type = 'file';
                input.accept = '*.json, application/json';
                input.addEventListener('change', function (event) {
                    if (event.target.files.length > 0) {
                        var file = event.target.files[0];
                        var reader = new FileReader();
                        reader.onload = function (event) {
                            if (event.target.readyState === 2) {
                                document.body.removeChild(input);
                                var result = JSON.parse(reader.result);
                                callback(result);
                            }
                        };
                        reader.readAsText(file);
                    }
                }, false);
                input.style.display = 'none';
                document.body.appendChild(input);
                input.click();
            }
        }
    });
