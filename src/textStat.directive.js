const tmpl = require('./partials/textStat.html');

angular
    .module('aetherworlds.sheet')
    .directive('aeTextStat', function () {
        return {
            restrict: 'E',
            scope: {
                name: '@',
                value: '='
            },
            template: tmpl
        };
    });
